//
//  VideoPlayerViewModel.swift
//  DevOpsPractical4
//
//  Created by Divyesh Vekariya on 27/02/24.
//

import AVFoundation
import UIKit

class VideoPlayerViewModel {
    
    var avPlayer: AVPlayerProtocol!
    var isPlaying = true
    var observer: Any?
    
    //MARK: - Initializer
    init(avPlayer: AVPlayerProtocol = AVPlayer()) {
        self.avPlayer = avPlayer
        startVideo()
        observeAppStateChanges()
    }
    
    deinit {
        releasePlayer()
    }
    
    //MARK: - Video Methods
    func startVideo() {
        let url = Bundle.main.url(forResource: "video", withExtension: "mp4")
        avPlayer = AVPlayer(url: url!)
        avPlayer.play()
    }
    
    func togglePlayback() {
        if isPlaying {
            avPlayer.pause()
        } else {
            avPlayer.play()
        }
        isPlaying.toggle()
    }
    
    //MARK: - App State Observation
    func observeAppStateChanges() {
        observer = NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: .main) { [weak self] (_) in
            self?.avPlayer.pause()
            self?.isPlaying = false
        }
        
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { [weak self] (_) in
            if self!.isPlaying {
                self?.avPlayer.play()
            } else {
                self?.avPlayer.pause()
            }
        }
    }
    
    //MARK: - Player Release
    func releasePlayer() {
        avPlayer.pause()
        avPlayer = nil
        
        if let observer = observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

protocol AVPlayerProtocol {
    var rate: Float { get }
    var error: Error? { get }
    
    func play()
    func pause()
    var isPlaying: Bool { get }
}

extension AVPlayer: AVPlayerProtocol {}

class MockAVPlayer: AVPlayerProtocol {
    var rate: Float = 0
    var error: Error?
    
    
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
    
    func play() {
        rate = 1.0
    }
    
    func pause() {
        rate = 0
    }
}
