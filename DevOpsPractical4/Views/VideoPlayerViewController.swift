//
//  VideoPlayerViewController.swift
//  DevOpsPractical4
//
//  Created by Divyesh Vekariya on 27/02/24.
//

import UIKit
import AVKit

import UIKit

class VideoPlayerViewController: UIViewController {
    
    @IBOutlet weak var videoView: UIView!
    
    //MARK: - Variable
    var viewModel: VideoPlayerViewModel?
    
    //MARK: - life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.avPlayer.play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.avPlayer.pause()
    }
    
    //MARK: - ViewModel Setup
    func setupViewModel() {
        viewModel = VideoPlayerViewModel()
        guard let viewModel = viewModel else { return }
        let playerLayer = AVPlayerLayer(player: viewModel.avPlayer as! AVPlayer)
        playerLayer.frame = videoView.bounds
        videoView.layer.addSublayer(playerLayer)
    }
    
    //MARK: - deinit
    deinit {
        viewModel?.releasePlayer()
    }
}
