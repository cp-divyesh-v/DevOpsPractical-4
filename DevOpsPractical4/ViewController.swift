//
//  ViewController.swift
//  Practical4
//
//  Created by Ishita on 27/06/23.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    
    //MARK: - @IBOutlets
    @IBOutlet weak var videoView: UIView!
    
    //MARK: - Variable
    var avPlayerController = AVPlayerViewController()
    var avPlayer: AVPlayer!
    var observer: Any?
    
    //MARK: - life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        startVideo()
        observeAppStateChanges()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        avPlayer.play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        avPlayer.pause()
    }
    
    deinit {
        releasePlayer()
    }
    
    func startVideo() {
        let url = Bundle.main.url(forResource: "video", withExtension: "mp4")
        avPlayer = AVPlayer(url: url!)
        avPlayerController.player = avPlayer
        avPlayerController.view.frame.size.height = videoView.frame.size.height
        avPlayerController.view.frame.size.width = videoView.frame.size.width
        videoView.addSubview(avPlayerController.view)
        avPlayer.play()
    }
    
    func observeAppStateChanges() {
        observer = NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: .main) { [weak self] (_) in
            self?.avPlayerController.player?.pause()
        }

        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { [weak self] (_) in
            self?.avPlayerController.player?.play()
        }
    }

    func releasePlayer() {
        avPlayerController.player?.pause()
        avPlayerController.player = nil
        avPlayerController.view.removeFromSuperview()
        avPlayerController.removeFromParent()
        
        if let observer = observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
}  
